package com.simbiotik.standard.productservice;

import com.simbiotik.standard.productservice.util.PdfToTiffConvertor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class ProductServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ProductServiceApplication.class, args);
//    try {
//      PdfToTiffConvertor.generateImageFromPDF(null, null);
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
    
//    try {
//      PdfToTiffConvertor.savePdfAsTiff();
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
  }

}
