/**
 *
 */
package com.simbiotik.standard.productservice.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import com.simbiotik.standard.productservice.config.DroolConfig;
import com.simbiotik.standard.productservice.dto.reponse.BasicPlanResponseModel;
import com.simbiotik.standard.productservice.dto.reponse.OfferResponseModel;
import com.simbiotik.standard.productservice.dto.request.BasicPlanRequestModel;
import com.simbiotik.standard.productservice.dto.request.CustomerRequestModel;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simbiotik.standard.common.service.ApplicationConstants;
import com.simbiotik.standard.productservice.dto.converter.ProductConverter;
import com.simbiotik.standard.productservice.dto.reponse.ProductResponseModel;
import com.simbiotik.standard.productservice.dto.request.ProductRequestModel;
import com.simbiotik.standard.productservice.entity.Product;
import com.simbiotik.standard.productservice.exceptions.ItemNotFoundException;
import com.simbiotik.standard.productservice.exceptions.RestApiException;
import com.simbiotik.standard.productservice.repository.ProductRepository;

/**
 * The product service class will be used for handle product operations
 *
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Service
@Transactional(readOnly = true)
public class ProductService {

  private ProductRepository productRepository;
  private ProductConverter productConverter;
  private MessageSource messageSource;
  private KieSession kieSession;

  /**
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param productRepository - initializing productRepository
   * @param productConverter - initializing productConverter
   * @param messageSource - initializing messageSource
   * @param kieSession - initializing kieSession
   */

  @Autowired
  public ProductService(ProductRepository productRepository, ProductConverter productConverter, MessageSource messageSource, KieSession kieSession) {
    this.productRepository = productRepository;
    this.productConverter = productConverter;
    this.messageSource = messageSource;
    this.kieSession = kieSession;
  }

  /**
   * This method is used to save the product
   *
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param productRequestModel - productRequestModel dto
   * @return converted ProductResponseModel
   */

  @Transactional(readOnly = false)
  public ProductResponseModel saveProduct(ProductRequestModel productRequestModel) {
    validate(productRequestModel);
    Product product = productConverter.productDtoToProduct(productRequestModel);
    product = productRepository.save(product);
    return productConverter.productToProductDto(product);
  }

  /**
   * This method validating whether product id existing already
   *
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param productRequestModel - used to validate existing product
   * @throws RestApiException if product id is already exist
   * @return nothing
   */

  private void validate(ProductRequestModel productRequestModel) {
    Optional<String> product = productRepository.findProductIdByProductId(productRequestModel.getProductId());
    if (product.isPresent()) {
      throw new RestApiException(HttpStatus.NOT_ACCEPTABLE,
          messageSource.getMessage("product.already.with.productid.exist", new Object[] { productRequestModel.getProductId() }, Locale.getDefault()));
    }
  }

  /**
   * Returns Product {@literal entity} for a given {@literal id}.
   *
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param id must not be null
   * @return productResponseModel
   * @throws ItemNotFoundException if an {@literal entity} for given
   *           {@literal id} is not found.
   */
  @Transactional(readOnly = true)
  public ProductResponseModel getProductById(Long id) throws ItemNotFoundException {
    Product product = productRepository.findById(id)
        .orElseThrow(() -> new ItemNotFoundException(messageSource.getMessage(ApplicationConstants.PRODUCT_NOT_FOUND_FOR_ID, new Object[] { id }, Locale.getDefault())));
    return productConverter.productToProductDto(product);
  }

  /**
   * Updates a record with given values for the given id
   *
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param productRequstModel - productRequstModel dto
   * @return productResponseModel
   * @throws ItemNotFoundException if an {@literal entity} for given
   *           {@literal id} is not found.
   */
  @Transactional(readOnly = false)
  public ProductResponseModel updateProduct(ProductRequestModel productRequstModel) throws ItemNotFoundException {
    Product product = productRepository.findById(productRequstModel.getId()).orElseThrow(
        () -> new ItemNotFoundException(messageSource.getMessage(ApplicationConstants.PRODUCT_NOT_FOUND_FOR_ID, new Object[] { productRequstModel.getId() }, Locale.getDefault())));
    updateProductDetails(product, productRequstModel);
    product = productRepository.save(product);
    return productConverter.productToProductDto(product);
  }

  /**
   * Deletes a record for the given id
   *
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param id must not be null
   * @return success if deleted the product
   * @throws ItemNotFoundException if an {@literal entity} for given
   *           {@literal id} is not found.
   */
  @Transactional(readOnly = false)
  public boolean deleteProduct(Long id) throws ItemNotFoundException {
    Product product = productRepository.findById(id)
        .orElseThrow(() -> new ItemNotFoundException(messageSource.getMessage(ApplicationConstants.PRODUCT_NOT_FOUND_FOR_ID, new Object[] { id }, Locale.getDefault())));
    productRepository.delete(product);
    return true;
  }

  /**
   * Returns a {@literal List} of all the product records
   *
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @return Set{@literal <ProductResponseModel>}
   */
  @Transactional(readOnly = true)
  public Set<ProductResponseModel> findAllProducts() {
    Set<ProductResponseModel> productResponseModels = new HashSet<>();
    List<Product> productList = productRepository.findAll();
    productList.forEach(product -> productResponseModels.add(productConverter.productToProductDto(product)));
    return productResponseModels;
  }

  /**
   * Sets the values to the product entity
   *
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param product - product entity
   * @param productRequestModel - productRequestModel dto
   * @return Product entity
   */
  public Product updateProductDetails(Product product, ProductRequestModel productRequestModel) {
    product.setProductId(productRequestModel.getProductId());
    product.setProductName(productRequestModel.getProductName());
    product.setPrice(productRequestModel.getPrice());
    product.setDescription(productRequestModel.getDescription());
    product.setShipmentType(productRequestModel.getShipmentType());
    return product;
  }

  /**
   * This method will be used for fetching all products of given list of ids
   *
   * @author pooja.b@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param productIds - list of ids
   * @return Product list of given list of ids
   */
  @Transactional(readOnly = true)
  public List<Product> getAllProductsByIds(List<Long> productIds) {
    List<Product> productList = productRepository.findAllById(productIds);
    return productList;
  }

  /**
   * calculate commission using drools
   *
   * @author pabasara@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param productRequestModel - ProductRequestModel dto
   * @return ProductResponseModel dto
   */
  public ProductResponseModel getCommission(ProductRequestModel productRequestModel){
    Product product = productConverter.productDtoToProduct(productRequestModel);
    ProductResponseModel productResponseModel = productConverter.productToProductDto(product);
    kieSession.insert(productResponseModel);
    kieSession.fireAllRules();
    kieSession.dispose();
    return productResponseModel;
  }

  /**
   * find an offer for the customer using drools
   *
   * @author pabasara@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param customerRequestModel - CustomerRequestModel dto
   * @return OfferResponseModel dto
   */
  public OfferResponseModel getOffer(CustomerRequestModel customerRequestModel){
    OfferResponseModel offerResponseModel = new OfferResponseModel();
    kieSession.setGlobal("offer", offerResponseModel);

    kieSession.insert(customerRequestModel);
    kieSession.fireAllRules();
    kieSession.dispose();
    return offerResponseModel;
  }

  /**
   * find basic plan using drools
   *
   * @author pabasara@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param basicPlanRequestModel - BasicPlanRequestModel dto
   * @return BasicPlanResponseModel dto
   */
  public BasicPlanResponseModel getBasicPlan(BasicPlanRequestModel basicPlanRequestModel){
    BasicPlanResponseModel basicPlanResponseModel = new BasicPlanResponseModel();
    kieSession.setGlobal("basicPlan", basicPlanResponseModel);

    kieSession.insert(basicPlanRequestModel);
    kieSession.fireAllRules();
    kieSession.dispose();
    return basicPlanResponseModel;
  }

}
