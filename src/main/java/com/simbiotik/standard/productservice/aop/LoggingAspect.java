package com.simbiotik.standard.productservice.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    @Pointcut("within(com.simbiotik.standard.productservice.service..*)")
    public void servicePackagePointcut(){}


    @Before("servicePackagePointcut()")
    public void logBefore(JoinPoint joinPoint){
        log.debug("Method {}.{}() is going to start..", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName());
    }

    @After("servicePackagePointcut()")
    public void logAfter(JoinPoint joinPoint){
        log.debug("Method {}.{}() end.", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName());
    }

    @AfterThrowing(pointcut = "servicePackagePointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e){
        log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName(), e.getMessage() != null ? e.getMessage() : "NULL");
    }
}
