/**
 * 
 */
package com.simbiotik.standard.productservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.simbiotik.standard.productservice.dto.reponse.ProductResponseModel;
import com.simbiotik.standard.productservice.entity.Product;

/**
 * The Product Repository interface will be used for storing product details
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

  Optional<String> findProductIdByProductId(String productId);

  List<ProductResponseModel> findAllById(Long id);

}
