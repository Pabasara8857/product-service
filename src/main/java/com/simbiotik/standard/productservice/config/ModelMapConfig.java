/**
 * 
 */
package com.simbiotik.standard.productservice.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The ModelMapConfig class will be used for configuration
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Configuration
public class ModelMapConfig {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
