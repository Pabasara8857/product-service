package com.simbiotik.standard.productservice.config;

import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;

/**
 * The ApiDocumentationConfig class will be used for configuration
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
public class ApiDocumentationConfig {
  @Bean
  public OpenAPI customOpenAPI(/*
                                * @Value("${application-description}") String
                                * appDesciption,
                                * 
                                * @Value("${application-version}") String
                                * appVersion
                                */) {
    return new OpenAPI().info(new Info().title("Product Service API").version("1.0.0").contact(new Contact().name("Saurabh Gupta").email("saurabh@simbiotiktech.com"))
        .description("Product Service API").termsOfService("http://hla.io/terms/").license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html")))
        .addServersItem(new Server().url("https://hla.uat.com").description("UAT Server").url("https://hla.prod.com").description("Production Server"));
  }

}
