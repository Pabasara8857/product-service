package com.simbiotik.standard.productservice.exceptions;

import org.springframework.http.HttpStatus;

/**
 * The Rest Api Exception class will be used to show status on occurance of
 * exeception
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
public class RestApiException extends RuntimeException {

  private static final long serialVersionUID = 6384563595507942374L;

  private final HttpStatus httpStatus;

  /**
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param httpStatus - httpStatus
   * @param message - exception message
   */
  public RestApiException(HttpStatus httpStatus, String message) {
    super(message);
    this.httpStatus = httpStatus;
  }

  /**
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param httpStatus - httpStatus
   * @param message - exception message
   * @param cause - cause of exception
   */
  public RestApiException(HttpStatus httpStatus, String message, Throwable cause) {
    super(message, cause);
    this.httpStatus = httpStatus;
  }

  /**
   * 
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @return the httpStatus
   */
  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

}
