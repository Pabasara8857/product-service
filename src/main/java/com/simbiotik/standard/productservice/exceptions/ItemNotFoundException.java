/**
 * 
 */
package com.simbiotik.standard.productservice.exceptions;

/**
 * The Item Not Found Exception class will be used to show messages on occurance
 * of exeception
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
public class ItemNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3098669988509514800L;

	public ItemNotFoundException(String message) {
		super(message);
	}

	public ItemNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
