package com.simbiotik.standard.productservice.exceptions;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.simbiotik.standard.productservice.dto.reponse.ApplicationExceptionResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * The Application ResponseException Handler class will be used to handle
 * exceptions occurring in operations
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@RestControllerAdvice
public class ApplicationResponseExceptionHandler extends ResponseEntityExceptionHandler {

  /**
   * This method is used to handle item not found exception
   * 
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param ex - ItemNotFoundException
   * @param request - web request
   * @return exceptionResponse for item not found
   */
	@ExceptionHandler(ItemNotFoundException.class)
	public final @ResponseBody ResponseEntity<ApplicationExceptionResponse> handleItemNotFoundExceptionException(
			ItemNotFoundException ex, WebRequest request) {
		String errorMessage = ex.getLocalizedMessage();
		if (errorMessage == null)
			errorMessage = ex.toString();
		ApplicationExceptionResponse exceptionResponse = new ApplicationExceptionResponse(errorMessage,
				LocalDateTime.now(), request.getDescription(false), HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
    /**
     * This method is used to handle Rest Api Exception
     * 
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param ex - RestApiException
     * @param request - web request
     * @return exceptionResponse item not found
     */
	@ExceptionHandler(RestApiException.class)
    public final @ResponseBody ResponseEntity<ApplicationExceptionResponse> handleItemNotFoundExceptionException(
        RestApiException ex, WebRequest request) {
        String errorMessage = ex.getLocalizedMessage();
        if (errorMessage == null)
            errorMessage = ex.toString();
        ApplicationExceptionResponse exceptionResponse = new ApplicationExceptionResponse(errorMessage,
                LocalDateTime.now(), request.getDescription(false), HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
    }

    /**
     * This method is used to handle all Exception
     * 
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param ex - Exception
     * @param request - web request
     * @return exceptionResponse for internal server error
     */
	@ExceptionHandler(Exception.class)
	public final @ResponseBody ResponseEntity<ApplicationExceptionResponse> handleAllException(Exception ex,
			WebRequest request) {
		String errorMessage = ex.getLocalizedMessage();
		if (errorMessage == null)
			errorMessage = ex.toString();
		ApplicationExceptionResponse exceptionResponse = new ApplicationExceptionResponse(errorMessage,
				LocalDateTime.now(), request.getDescription(false), HttpStatus.INTERNAL_SERVER_ERROR.value());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

//    @ExceptionHandler(TokenNotValidException.class)
//    public final ResponseEntity<ApplicationExceptionResponse> handleTokenNotValidException
//            (TokenNotValidException ex, WebRequest request) throws Exception {
//        String errorMessage  = ex.getLocalizedMessage();
//        if(errorMessage == null) errorMessage = ex.toString();
//        ApplicationExceptionResponse exceptionResponse = new ApplicationExceptionResponse(errorMessage,
//                LocalDateTime.now(), request.getDescription(false));
//        return new ResponseEntity(exceptionResponse, HttpStatus.UNAUTHORIZED);
//    }

/**
 * This method is used to handle Method Argument Not Valid Exception
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 * @param ex - MethodArgumentNotValidException
 * @param headers - HttpHeaders
 * @param status - HttpStatus
 * @param request - web request
 * @return exceptionResponse for bad request
 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
		ApplicationExceptionResponse exceptionResponse = new ApplicationExceptionResponse(errors,
				LocalDateTime.now(), request.getDescription(false), HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}

}
