/**
 *
 */
package com.simbiotik.standard.productservice.resource;

import com.simbiotik.standard.common.service.ApplicationConstants;
import com.simbiotik.standard.productservice.dto.reponse.BaseResponse;
import com.simbiotik.standard.productservice.dto.reponse.BasicPlanResponseModel;
import com.simbiotik.standard.productservice.dto.reponse.OfferResponseModel;
import com.simbiotik.standard.productservice.dto.reponse.ProductResponseModel;
import com.simbiotik.standard.productservice.dto.request.BasicPlanRequestModel;
import com.simbiotik.standard.productservice.dto.request.CustomerRequestModel;
import com.simbiotik.standard.productservice.dto.request.ProductRequestModel;
import com.simbiotik.standard.productservice.entity.Product;
import com.simbiotik.standard.productservice.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.links.Link;
import io.swagger.v3.oas.annotations.links.LinkParameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * The product resource class will be used for control product operations
 *
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@RequestMapping("/products")
@RestController
//@CrossOrigin(origins = "http://localhost:8123")
public class ProductResource {

    private ProductService productService;
    private MessageSource messageSource;

    /**
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param productService - initializing productService
     * @param messageSource - initializing messageSource
     *
     */
    @Autowired(required = true)
    public ProductResource(ProductService productService, MessageSource messageSource) {
        this.productService = productService;
        this.messageSource = messageSource;
    }

    /**
     * This method is used to create new product
     *
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param productRequestModel - the details of new product which we want to
     *          add
     * @return baseResponse
     */

    @Operation(summary = "Create Product", security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER")) @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "Created the product", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponseModel.class)) }, links = {
            @Link(name = "Find product by id", operationId = "getProductById", parameters = { @LinkParameter(name = "id", expression = "$request.query.id") }) }),
        @ApiResponse(responseCode = "400", description = "Invalid product id", content = @Content),
        @ApiResponse(responseCode = "404", description = "Product not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content) }) @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> saveProduct(
        @Valid @RequestBody ProductRequestModel productRequestModel) {
        ProductResponseModel productResponseModel = productService.saveProduct(productRequestModel);
        productResponseModel.add(linkTo(methodOn(ProductResource.class).getProductById(productResponseModel.getId())).withRel(ApplicationConstants.GET),
            linkTo(methodOn(ProductResource.class).deleteProduct(productResponseModel.getId())).withRel(ApplicationConstants.DELETE),
            linkTo(methodOn(ProductResource.class).findAllProducts()).withRel(ApplicationConstants.GET_ALL));
        BaseResponse baseResponse = new BaseResponse(HttpStatus.CREATED.value(), messageSource.getMessage("product.saved.successfully", null, Locale.getDefault()),
            productResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
    }

    /**
     * This method is used to create new product
     *
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param id - id for which we should get the product
     * @return baseResponse
     */
    @Operation(summary = "Get Product by id", security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER")) @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the product", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponseModel.class)) }),
        @ApiResponse(responseCode = "400", description = "Invalid product id", content = @Content),
        @ApiResponse(responseCode = "404", description = "Product not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content) }) @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> getProductById(
        @PathVariable("id") Long id) {
        ProductResponseModel productResponseModel = productService.getProductById(id);
        productResponseModel.add(linkTo(methodOn(ProductResource.class).getProductById(productResponseModel.getId())).withRel(ApplicationConstants.GET),
            linkTo(methodOn(ProductResource.class).deleteProduct(productResponseModel.getId())).withRel(ApplicationConstants.DELETE));
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("product.found.successfully", null, Locale.getDefault()),
            productResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to fetch all products
     *
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return baseResponse
     */
    @Operation(summary = "Get all Products", security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER")) @ApiResponses(value = {
        @ApiResponse(responseCode = "400", description = "Invalid product id", content = @Content),
        @ApiResponse(responseCode = "404", description = "Product not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content) }) @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> findAllProducts() {
        Set<ProductResponseModel> productResponseModels = productService.findAllProducts();
        productResponseModels.forEach(productResponseModel -> {
            productResponseModel.add(linkTo(methodOn(ProductResource.class).getProductById(productResponseModel.getId())).withRel(ApplicationConstants.GET),
                linkTo(methodOn(ProductResource.class).deleteProduct(productResponseModel.getId())).withRel(ApplicationConstants.DELETE));
        });
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("product.fetched.successfully", null, Locale.getDefault()),
            productResponseModels);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to update the existing product
     *
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param productRequestModel - the details of product which we want to update
     * @return baseResponse
     */
    @Operation(summary = "Update Product", security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER")) @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Updated the product", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = ProductResponseModel.class)) }),
        @ApiResponse(responseCode = "404", description = "Product not found", content = @Content),
        @ApiResponse(responseCode = "400", description = "Invalid product id", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content) }) @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> updateProduct(
        @Valid @RequestBody ProductRequestModel productRequestModel) {
        ProductResponseModel productResponseModel = productService.updateProduct(productRequestModel);
        productResponseModel.add(linkTo(methodOn(ProductResource.class).getProductById(productResponseModel.getId())).withRel(ApplicationConstants.GET),
            linkTo(methodOn(ProductResource.class).deleteProduct(productResponseModel.getId())).withRel(ApplicationConstants.DELETE),
            linkTo(methodOn(ProductResource.class).findAllProducts()).withRel(ApplicationConstants.GET_ALL));
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("product.updated.successfully", null, Locale.getDefault()),
            productResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to delete existing product
     *
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param id - the id of product which we want to delete
     * @return baseResponse
     */
    @Operation(summary = "Delete Product", security = @SecurityRequirement(name = "ALLOWED_ROLE", scopes = "ROLE_USER")) @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Deleted the product", content = @Content),
        @ApiResponse(responseCode = "404", description = "Product not found", content = @Content),
        @ApiResponse(responseCode = "400", description = "Invalid product id", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content) }) @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> deleteProduct(
        @PathVariable("id") Long id) {
        Boolean isDeleted = productService.deleteProduct(id);
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("product.deleted.successfully", null, Locale.getDefault()), isDeleted);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method will be used for fetching all products of given list of ids
     *
     * @author pooja.b@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param productIds - list of ids
     * @return baseResponse
     */
    @GetMapping(value = "/getAllProductsByIds/{productIds}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> getAllProductsByIds(
        @PathVariable List<Long> productIds) {
        List<Product> productList = productService.getAllProductsByIds(productIds);
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("product.fetched.successfully", null, Locale.getDefault()), productList);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to get commission
     *
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param productRequestModel - ProductRequestModel dto
     * @return baseResponse
     */
    @PostMapping(value = "/commission", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> getCommission(
        @Valid @RequestBody ProductRequestModel productRequestModel) {
        ProductResponseModel productResponseModel = productService.getCommission(productRequestModel);
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), ApplicationConstants.PRODUCT, productResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to get an offer for the customer
     *
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param customerRequestModel - CustomerRequestModel dto
     * @return baseResponse
     */
    @PostMapping(value = "/offer", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> getOffer(
        @Valid @RequestBody CustomerRequestModel customerRequestModel) {
        OfferResponseModel offerResponseModel = productService.getOffer(customerRequestModel);
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), ApplicationConstants.PRODUCT, offerResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to get basic plan
     *
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param basicPlanRequestModel - BasicPlanRequestModel dto
     * @return baseResponse
     */
    @PostMapping(value = "/basicPlan", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> getBasicPlan(
        @Valid @RequestBody BasicPlanRequestModel basicPlanRequestModel) {
        BasicPlanResponseModel basicPlanResponseModel = productService.getBasicPlan(basicPlanRequestModel);
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), ApplicationConstants.PRODUCT, basicPlanResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @Value("${environment.property}") private String environment;

    @GetMapping("/property-env") public ResponseEntity<String> getEnvironmentProperty() {
        return ResponseEntity.ok(environment);
    }

}
