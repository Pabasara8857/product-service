/**
 * 
 */
package com.simbiotik.standard.productservice.enums;

/**
 * The Status enum will be used for showing Status of prouct
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
public enum Status {
	ACTIVE, INACTIVE
}
