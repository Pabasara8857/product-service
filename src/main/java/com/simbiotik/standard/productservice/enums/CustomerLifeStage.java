package com.simbiotik.standard.productservice.enums;

public enum CustomerLifeStage {
    GETTINGSTARTED,
    CAREERFOCUSED,
    ADVICEFAMILY,
    EMPTYNESTER,
    GOLDENYEARS,
    BUSINESS
}
