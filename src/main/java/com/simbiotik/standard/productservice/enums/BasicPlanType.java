package com.simbiotik.standard.productservice.enums;

public enum BasicPlanType {
    BASIC_PREMIUM,
    DEATH_BENEFIT
}
