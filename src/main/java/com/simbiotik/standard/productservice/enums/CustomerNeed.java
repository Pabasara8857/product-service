package com.simbiotik.standard.productservice.enums;

public enum CustomerNeed {
    SAVINGACCOUNT,
    LIFEINSURANCE,
    DIGITALBANKING,
    STUDENTLOAN,
    MORTAGE
}
