package com.simbiotik.standard.productservice.enums;

public enum CustomerAssets {
    TO50K,
    FROM50KTO150K,
    FROM150KTO300K,
    OVER300K
}
