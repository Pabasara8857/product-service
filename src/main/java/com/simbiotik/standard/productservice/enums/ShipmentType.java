/**
 * 
 */
package com.simbiotik.standard.productservice.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The ShipmentType enum will be used for showing types of shipment
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
public enum ShipmentType {
	
	LOCAL(0, "Local"), INTERNATIONAL(1, "International");

	  private final int code;

	  private final String name;

	  ShipmentType(int code, String name) {
	    this.code = code;
	    this.name = name;
	  }

	  public int getCode() {
	    return code;
	  }

	  public String getName() {
	    return name;
	  }

	  private static final Map<Integer, ShipmentType> LOOKUP = new HashMap<Integer, ShipmentType>();

	  static {
	    for (ShipmentType shipmentType : EnumSet.allOf(ShipmentType.class)) {
	      LOOKUP.put(shipmentType.getCode(), shipmentType);
	    }
	  }

	  public static ShipmentType fromCode(int code) {
	    return LOOKUP.get(code);
	  }
	  
}
