package com.simbiotik.standard.productservice.enums;

public enum ProductPackage {
    GETTINGSTARTED_PACKAGE,
    CAREERFOCUSED_PACKAGE,
    ADVICEFAMILY_PACKAGE,
    EMPTYNESTER_PACKAGE,
    GOLDENYEARS_PACKAGE,
    BUSINESS_PACKAGE
}
