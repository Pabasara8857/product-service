package com.simbiotik.standard.productservice.enums;

public enum Product {
    LOAN,
    SUPERLOAN,
    INSURANCE
}
