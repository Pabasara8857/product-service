/**
 * 
 */
package com.simbiotik.standard.productservice.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.springframework.core.io.ClassPathResource;

import com.icafe4j.image.ImageColorType;
import com.icafe4j.image.ImageParam;
import com.icafe4j.image.options.TIFFOptions;
import com.icafe4j.image.quant.DitherMatrix;
import com.icafe4j.image.quant.DitherMethod;
import com.icafe4j.image.tiff.TIFFTweaker;
import com.icafe4j.image.tiff.TiffFieldEnum.Compression;
import com.icafe4j.io.FileCacheRandomAccessOutputStream;

import lombok.extern.log4j.Log4j2;

/**
 * This class will be used for converting pdf file to tiff file
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Log4j2
public class PdfToTiffConvertor {

  private PdfToTiffConvertor() {
  }

  /**
   * This method is used for generating image from file
   * 
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param filename - file to be converted into image
   * @param extension - file extension
   * @throws IOException - throws IOException
   * 
   */
  public static void generateImageFromPDF(String filename, String extension) throws IOException {
    filename = "unflatten";
    InputStream is = new ClassPathResource("/" + filename + ".pdf").getInputStream();
    PDDocument document = PDDocument.load(is);
    PDFRenderer pdfRenderer = new PDFRenderer(document);
    // List<BufferedImage> bufferedImages = new ArrayList<>();
    // int width = 0;
    // int height = 0;
    for (int page = 0; page < document.getNumberOfPages(); ++page) {
      BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 250, ImageType.RGB);
      ImageIOUtil.writeImage(bim, "src/main/resources/" + filename + "-250dpi-" + (page + 1) + ".tiff", 300);
      // bufferedImages.add(bim);
      // height = height + bim.getHeight();
      // width = bim.getWidth();
      log.info("Image created successfully.");
    }

    // BufferedImage newImage = joinBufferedImage(bufferedImages,
    // document.getNumberOfPages(), width, height);
    // ImageIOUtil.writeImage(newImage, "src/main/resources/my_image.tiff",
    // 300);

    log.info("Image created completed.");

    document.close();
    is.close();
  }

  /**
   * This method is used for concatenate buffered images
   * 
   * @author saurabh@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param bufferedImages - file to be joined
   * @param offset - offset
   * @param width - width of buffered image
   * @param height - height of buffered image
   * @return new image having concatenated buffered image
   * 
   */
  private static BufferedImage joinBufferedImage(List<BufferedImage> bufferedImages, int offset, int width, int height) {
    BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2 = newImage.createGraphics();
    Color oldColor = g2.getColor();
    g2.setPaint(Color.WHITE);
    g2.fillRect(0, 0, width, height);
    g2.setColor(oldColor);
    int heightCurr = 0;
    for (int i = 0; i < bufferedImages.size(); i++) {
      g2.drawImage(bufferedImages.get(i), 0, heightCurr, null);
      heightCurr += bufferedImages.get(i).getHeight();
    }
    g2.dispose();
    return newImage;
  }

  public static void savePdfAsTiff() throws IOException {
    String filename = "unflatten";
    InputStream is = new ClassPathResource("/" + filename + ".pdf").getInputStream();
    PDDocument pddoc = PDDocument.load(is);
    PDFRenderer pdfRenderer = new PDFRenderer(pddoc);
    BufferedImage[] images = new BufferedImage[pddoc.getNumberOfPages()];
    for (int i = 0; i < images.length; i++) {
      try {
        BufferedImage bim = pdfRenderer.renderImageWithDPI(i, 300, ImageType.RGB);
        images[i] = bim;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream("src/main/resources/" + filename + "-1.tiff");
      FileCacheRandomAccessOutputStream rout = new FileCacheRandomAccessOutputStream(fos);
      ImageParam.ImageParamBuilder builder = ImageParam.getBuilder();
      ImageParam[] param = new ImageParam[1];
      TIFFOptions tiffOptions = new TIFFOptions();
      tiffOptions.setTiffCompression(Compression.CCITTFAX4);
      tiffOptions.setXResolution(300);
      tiffOptions.setYResolution(300);
      builder.imageOptions(tiffOptions);
      builder.colorType(ImageColorType.BILEVEL).ditherMatrix(DitherMatrix.getBayer8x8Diag()).applyDither(true).ditherMethod(DitherMethod.BAYER);
      param[0] = builder.build();
      TIFFTweaker.writeMultipageTIFF(rout, images, param);
      rout.close();
      System.out.println("Image created");
    } catch (Exception e) {
    } finally {
      fos.close();
    }
  }

}
