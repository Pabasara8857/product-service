/**
 * 
 */
package com.simbiotik.standard.productservice.dto.request;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simbiotik.standard.productservice.enums.ShipmentType;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The ProductRequestModel class will be used to represent product request model
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Component
@Data
@EqualsAndHashCode()
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "Product Request Model", description = "Represent product request model")
public class ProductRequestModel implements Serializable {

  @JsonProperty("id")
	@Schema(title = "This field is to enter product id", description = "Unique identifier of the Product.", required = true)
  private Long id = null;

  @NotNull(message = "productId cannot be null")
  @NotBlank(message = "productId cannot be blank")
	@Schema(title = "This field is to enter productId", description = "ProductId of the Product.", required = true)
  @Size(max = 50)
  @JsonProperty("productId")
  private String productId;

  @NotNull(message = "productName cannot be null")
  @NotBlank(message = "productName cannot be blank")
	@Schema(title = "This field is to enter product name", description = "Name of the Product", required = true)
  @Size(max = 100)
  @JsonProperty("productName")
  private String productName;

  @NotNull(message = "price cannot be null")
	@Schema(title = "This field is to enter product price", description = "Price of the Product", required = true)
  @JsonProperty("price")
  private BigDecimal price;

  @NotNull(message = "shipmentType cannot be null")
	@Schema(title = "This field is to enter shipment type", description = "Shipment Type of the Product", required = true)
  @JsonProperty("shipmentType")
  private ShipmentType shipmentType;

	@Schema(title = "This field is to enter product description", description = "Description of the Product")
  @Size(max = 500)
  @JsonProperty("description")
  private String description;

}
