package com.simbiotik.standard.productservice.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simbiotik.standard.productservice.enums.BasicPlanType;
import com.simbiotik.standard.productservice.enums.CustomerAssets;
import com.simbiotik.standard.productservice.enums.CustomerLifeStage;
import com.simbiotik.standard.productservice.enums.CustomerNeed;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.List;

@Component
@Data
@EqualsAndHashCode()
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "Basic Plan Request Model", description = "Represent basic plan request model")
public class BasicPlanRequestModel {

    @Schema(title = "This field is to enter Basic Plan", description = "Basic Plan", required = false)
    @JsonProperty("basicPlan")
    private BasicPlanType basicPlan;

    @Schema(title = "This field is to enter Premium Rate", description = "Premium Rate", required = false)
    @JsonProperty("premiumRate")
    private double premiumRate;

    @Schema(title = "This field is to enter Basic Sum Assured", description = "Basic Sum Assured", required = false)
    @JsonProperty("basicSumAssured")
    private double basicSumAssured;

    @Schema(title = "This field is to enter Multiply Factor", description = "Multiply Factor", required = false)
    @JsonProperty("multiplyFactor")
    private double multiplyFactor;

    @Schema(title = "This field is to enter Health Loading", description = "Health Loading", required = false)
    @JsonProperty("healthLoading")
    private double healthLoading;

    @Schema(title = "This field is to enter Temporary Health Loading", description = "Temporary Health Loading", required = false)
    @JsonProperty("temporaryHealthLoading")
    private double temporaryHealthLoading;

    @Schema(title = "This field is to enter Occupation Loading", description = "Occupation Loading", required = false)
    @JsonProperty("occupationLoading")
    private double occupationLoading;

    @Schema(title = "This field is to enter LSD Rate", description = "LSD Rate", required = false)
    @JsonProperty("lsdRate")
    private double lsdRate;

    @Schema(title = "This field is to enter Juvenile Lien Factor", description = "Juvenile Lien Factor", required = false)
    @JsonProperty("juvenileLienFactor")
    private double juvenileLienFactor;

}
