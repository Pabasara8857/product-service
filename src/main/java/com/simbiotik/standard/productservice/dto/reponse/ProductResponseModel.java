/**
 * 
 */
package com.simbiotik.standard.productservice.dto.reponse;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simbiotik.standard.productservice.enums.ShipmentType;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The ProductRequestModel class will be used to represent product response
 * model
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Schema(title = "Product Response Model", description = "Represent product response model")
public class ProductResponseModel extends RepresentationModel<ProductResponseModel> implements Serializable {

	@Schema(title = "product id", description = "Auto generated Id")
	@JsonProperty("id")
	private Long id;

	@Schema(title = "ProductId", description = "Max allowed 50 characters")
	@JsonProperty("productId")
	private String productId;

	@Schema(title = "Product name", description = " Max allowed 100 characters")
	@JsonProperty("productName")
	private String productName;

	@Schema(title = "price", description = "Max allowed 15 characters")
	@JsonProperty("price")
	private BigDecimal price;

	@Schema(title = "Shipment Type", description = " Max allowed 100 characters")
	@JsonProperty("shipmentType")
    private ShipmentType shipmentType;
	
	@Schema(title = "description", description = "Max allowed 500 characters")
	@JsonProperty("description")
	private String description;

	@JsonProperty("commission")
	private int commission;
}
