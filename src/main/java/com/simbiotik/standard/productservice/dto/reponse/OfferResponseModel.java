package com.simbiotik.standard.productservice.dto.reponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simbiotik.standard.productservice.enums.Product;
import com.simbiotik.standard.productservice.enums.ProductPackage;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Schema(title = "Offer Response Model", description = "Represent offer response model")
public class OfferResponseModel {

    @Schema(title = "Financial package", description = "Selected financial package for the customer")
    @JsonProperty("financialPackage")
    private ProductPackage financialPackage;

    @Schema(title = "Products", description = "Selected products for the customer")
    @JsonProperty("products")
    private List<Product> products = new ArrayList<>();

    @Schema(title = "Discount", description = "Calculated discount for the customer")
    @JsonProperty("discount")
    private int discount;

    public void addSingleProduct(Product prod){
        this.products.add(prod);
    }
}
