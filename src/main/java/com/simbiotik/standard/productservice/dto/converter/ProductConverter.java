/**
 * 
 */
package com.simbiotik.standard.productservice.dto.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simbiotik.standard.productservice.dto.reponse.ProductResponseModel;
import com.simbiotik.standard.productservice.dto.request.ProductRequestModel;
import com.simbiotik.standard.productservice.entity.Product;

import lombok.NoArgsConstructor;

/**
 * The ProductConverter class will be used to convert entity to dto and dto to
 * entity
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class ProductConverter {
	
	private ModelMapper modelMapper;

    @Autowired
    public ProductConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    /**
     * This method is used to convert product dto to product entity
     * 
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param productRequestModel - productRequestModel dto
     * @return product entity
     */
    public Product productDtoToProduct(ProductRequestModel productRequestModel){
        return this.modelMapper.map(productRequestModel, Product.class);
    }

    /**
     * This method is used to convert product product entity to dto
     * 
     * @author saurabh@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param product - product entity
     * @return product dto
     */
    public ProductResponseModel productToProductDto(Product product){
        return this.modelMapper.map(product, ProductResponseModel.class);
    }

}
