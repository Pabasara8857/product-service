package com.simbiotik.standard.productservice.dto.reponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simbiotik.standard.productservice.enums.Product;
import com.simbiotik.standard.productservice.enums.ProductPackage;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Schema(title = "Basic Plan Response Model", description = "Represent basic plan response model")
public class BasicPlanResponseModel {

    @Schema(title = "Basic Premium")
    @JsonProperty("basicPremium")
    private double basicPremium;

    @Schema(title = "Premium")
    @JsonProperty("premium")
    private double premium;

    @Schema(title = "Health Loading")
    @JsonProperty("healthLoading")
    private double healthLoading;

    @Schema(title = "Occupation Loading")
    @JsonProperty("occupationLoading")
    private double occupationLoading;

    @Schema(title = "LSD Rate")
    @JsonProperty("lsdRate")
    private double lsdRate;

    @Schema(title = "Death Benefit")
    @JsonProperty("deathBenefit")
    private double deathBenefit;

    public void setBasicPremium(double basicPremium) {
        this.basicPremium = setToTwoDecimalValues(basicPremium);
    }

    public void setPremium(double premium) {
        this.premium = setToTwoDecimalValues(premium);
    }

    public void setHealthLoading(double healthLoading) {
        this.healthLoading = setToTwoDecimalValues(healthLoading);
    }

    public void setOccupationLoading(double occupationLoading) {
        this.occupationLoading = setToTwoDecimalValues(occupationLoading);
    }

    public void setLsdRate(double lsdRate) {
        this.lsdRate = setToTwoDecimalValues(lsdRate);
    }

    public void setDeathBenefit(double deathBenefit) {
        this.deathBenefit = setToTwoDecimalValues(deathBenefit);
    }

    public double setToTwoDecimalValues(double value){
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(value));
    }
}
