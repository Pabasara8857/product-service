package com.simbiotik.standard.productservice.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simbiotik.standard.productservice.enums.CustomerAssets;
import com.simbiotik.standard.productservice.enums.CustomerLifeStage;
import com.simbiotik.standard.productservice.enums.CustomerNeed;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
@EqualsAndHashCode()
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "Customer Request Model", description = "Represent customer request model")
public class CustomerRequestModel {

    @Schema(title = "This field is to enter life stage of the customer", description = "Customer's life stage", required = false)
    @JsonProperty("lifeStage")
    private CustomerLifeStage lifeStage;

    @Schema(title = "This field is to enter assets of the customer", description = "Customer's assets", required = false)
    @JsonProperty("assets")
    private CustomerAssets assets;

    @Schema(title = "This field is to enter customer's needs", description = "Customer's needs", required = false)
    @JsonProperty("customerNeeds")
    private List<CustomerNeed> customerNeeds;

}
