/**
 * 
 */
package com.simbiotik.standard.productservice.dto.reponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class is used as the base response dto to return the result
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse<T> {

	private int returnCode;
	private String message;
	private T result;
}
