/**
 * 
 */
package com.simbiotik.standard.productservice.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.simbiotik.standard.productservice.enums.ShipmentType;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The product class is used to define attributes product entity
 * 
 * @author saurabh@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "PRODUCT", indexes = { @Index(name = "INDEX_PRODUCT_ID", columnList = "PRODUCT_ID", unique = true),
		@Index(name = "INDEX_PRODUCT_NAME", columnList = "PRODUCT_NAME", unique = false),
		@Index(name = "INDEX_PRICE", columnList = "PRICE", unique = false) }, uniqueConstraints = {
				@UniqueConstraint(name = "UNI_PRODUCT_ID", columnNames = "PRODUCT_ID") })
public class Product extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 738103519913337777L;

	/**
     * auto generated id
     */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

    /**
     * product id
     */
	@NotNull
	@Column(name = "PRODUCT_ID", length = 50, nullable = false)
	private String productId;

    /**
     * product name
     */
	@NotNull
	@Column(name = "PRODUCT_NAME", length = 100, nullable = false)
	private String productName;

    /**
     * product price
     */
	@NotNull
	@Column(name = "PRICE", length = 15, nullable = false)
	private BigDecimal price = BigDecimal.ZERO;
	
    /**
     * shipment type
     */
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "SHIPMENT_TYPE", length = 100, nullable = false)
	private ShipmentType shipmentType = ShipmentType.LOCAL;

    /**
     * product description
     */
	@Column(name = "DESCRIPTION", length = 500, nullable = true)
	private String description;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
