package com.simbiotik.standard.productservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.testng.MockitoTestNGListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.simbiotik.standard.productservice.resource.ProductResource;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Listeners(MockitoTestNGListener.class)
public class ProductServiceApplicationTests extends AbstractTestNGSpringContextTests {

	@Autowired
	private ProductResource productResource;

	@Test(enabled = true)
	void contextLoads() {
		assertThat(productResource).isNotNull().describedAs("productResource is not null and is loaded");
	}
	
	@Test(enabled = true)
	public void check() {
		assertEquals("asdf", "asdf");
	}

}
