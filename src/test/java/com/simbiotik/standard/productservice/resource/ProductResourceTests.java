/**
 * 
 */
package com.simbiotik.standard.productservice.resource;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.mockito.testng.MockitoTestNGListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbiotik.standard.productservice.dto.reponse.ProductResponseModel;
import com.simbiotik.standard.productservice.dto.request.ProductRequestModel;
import com.simbiotik.standard.productservice.enums.ShipmentType;
import com.simbiotik.standard.productservice.service.ProductService;

/**
 * @author saurabh
 *
 */

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Listeners(MockitoTestNGListener.class)
public class ProductResourceTests extends AbstractTestNGSpringContextTests {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	@MockBean
	ProductService productService;

	@Autowired
	ObjectMapper objectMapper;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void saveProductTest() throws Exception {
		ProductRequestModel productRequestModel = new ProductRequestModel(null, "000001", "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen");
		ProductResponseModel productResponseModel = new ProductResponseModel(1L, "000001", "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen", 10);

		when(this.productService.saveProduct(any(ProductRequestModel.class))).thenReturn(productResponseModel);

		mockMvc.perform(post("/products").content(asJsonString(productRequestModel))
				.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.product.productId", is(productResponseModel.getProductId())))
				.andExpect(jsonPath("$.product.productName", is(productResponseModel.getProductName())))
//				.andExpect(jsonPath("$.product.price", is(productResponseModel.getPrice())))
//				.andExpect(jsonPath("$.product.shipmentType", is(productResponseModel.getShipmentType())))
				.andExpect(jsonPath("$.product.description", is(productResponseModel.getDescription())))
				.andExpect(jsonPath("$.message", is("Product saved successfully")));
	}

	@Test
	public void saveProduct400Test() throws Exception {
		ProductRequestModel productRequestModel = new ProductRequestModel(null, null, "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen");
		List<String> errorMesage = Stream.of("productId cannot be null", "productId cannot be blank")
				.collect(Collectors.toUnmodifiableList());
		List<String> errorMesage1 = Stream.of("productId cannot be blank", "productId cannot be null")
				.collect(Collectors.toUnmodifiableList());

		mockMvc.perform(post("/products").content(asJsonString(productRequestModel))
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print()).andExpect(status().is4xxClientError())
				.andExpect(jsonPath("$.errorMessage", anyOf(is(errorMesage), is(errorMesage1))));
	}

	@Test
	public void getProductByIdTest() throws Exception {
		ProductResponseModel productResponseModel = new ProductResponseModel(1L, "000001", "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen", 10);

		when(this.productService.getProductById(any(Long.class))).thenReturn(productResponseModel);

		mockMvc.perform(get("/products/1")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.product.productId", is(productResponseModel.getProductId())))
				.andExpect(jsonPath("$.product.productName", is(productResponseModel.getProductName())))
//				.andExpect(jsonPath("$.product.price", is(productResponseModel.getPrice())))
//				.andExpect(jsonPath("$.product.shipmentType", is(productResponseModel.getShipmentType())))
				.andExpect(jsonPath("$.product.description", is(productResponseModel.getDescription())));
	}

	@Test
	public void getProductById400Test() throws Exception {
		mockMvc.perform(get("/products/null")).andExpect(status().is4xxClientError());
	}

	@Test
	public void updateProductTest() throws Exception {
		ProductRequestModel productRequestModel = new ProductRequestModel(null, "000001", "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen");
		ProductResponseModel productResponseModel = new ProductResponseModel(1L, "000001", "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen", 10);

		when(this.productService.updateProduct(any(ProductRequestModel.class))).thenReturn(productResponseModel);

		mockMvc.perform(put("/products").content(asJsonString(productRequestModel))
				.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.product.productId", is(productResponseModel.getProductId())))
				.andExpect(jsonPath("$.product.productName", is(productResponseModel.getProductName())))
//				.andExpect(jsonPath("$.product.price", is(productResponseModel.getPrice())))
//				.andExpect(jsonPath("$.product.shipmentType", is(productResponseModel.getShipmentType())))
				.andExpect(jsonPath("$.product.description", is(productResponseModel.getDescription())))
				.andExpect(jsonPath("$.message", is("Product updated successfully")));
	}

	@Test
	public void updateProduct400Test() throws Exception {
		ProductRequestModel productRequestModel = new ProductRequestModel(null, null, "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen");
		List<String> errorMesage = Stream.of("productId cannot be null", "productId cannot be blank")
				.collect(Collectors.toUnmodifiableList());
		List<String> errorMesage1 = Stream.of("productId cannot be blank", "productId cannot be null")
				.collect(Collectors.toUnmodifiableList());

		mockMvc.perform(put("/products").content(asJsonString(productRequestModel))
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print()).andExpect(status().is4xxClientError())
				.andExpect(jsonPath("$.errorMessage", anyOf(is(errorMesage), is(errorMesage1))));
	}

	@Test
	public void deleteProductTest() throws Exception {
		when(this.productService.deleteProduct(any(Long.class))).thenReturn(true);

		mockMvc.perform(delete("/products/1")).andExpect(status().isOk()).andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.isDeleted", is(true)))
				.andExpect(jsonPath("$.message", is("Product deleted successfully")));
	}

	@Test
	public void deleteProduct400Test() throws Exception {
		mockMvc.perform(delete("/products/null")).andExpect(status().is4xxClientError()).andDo(print());
	}

	@Test
	public void findAllProductsTest() throws Exception {
		ProductResponseModel productResponseModel = new ProductResponseModel(1L, "000001", "Pen", new BigDecimal("5"),
				ShipmentType.LOCAL, "Ball Pen", 10);
		Set<ProductResponseModel> productRequestModels = Stream.of(productResponseModel)
				.collect(Collectors.toUnmodifiableSet());

		when(this.productService.findAllProducts()).thenReturn(productRequestModels);

		mockMvc.perform(get("/products")).andExpect(status().isOk()).andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.productList.[0].productId", is(productResponseModel.getProductId())))
				.andExpect(jsonPath("$.productList.[0].productName", is(productResponseModel.getProductName())))
//				.andExpect(jsonPath("$.productList.[0].price", is(productResponseModel.getPrice())))
//				.andExpect(jsonPath("$.productList.[0].shipmentType", is(productResponseModel.getShipmentType())))
				.andExpect(jsonPath("$.productList.[0].description", is(productResponseModel.getDescription())));
	}

	@Test
	public void findAllProducts415Test() throws Exception {
		mockMvc.perform(post("/products")).andExpect(status().is4xxClientError()).andDo(print());
	}

	@AfterMethod
	public void afterMethod() {
		reset(productService);
	}

	public String asJsonString(final Object obj) {
		try {
			return this.objectMapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
