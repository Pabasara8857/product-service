/**
 * 
 */
package com.simbiotik.standard.productservice.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.testng.MockitoTestNGListener;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.simbiotik.standard.productservice.dto.converter.ProductConverter;
import com.simbiotik.standard.productservice.dto.reponse.ProductResponseModel;
import com.simbiotik.standard.productservice.dto.request.ProductRequestModel;
import com.simbiotik.standard.productservice.entity.Product;
import com.simbiotik.standard.productservice.enums.ShipmentType;
import com.simbiotik.standard.productservice.exceptions.ItemNotFoundException;
import com.simbiotik.standard.productservice.repository.ProductRepository;

/**
 * @author saurabh
 *
 */

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Listeners(MockitoTestNGListener.class)
public class ProductServiceTests extends AbstractTestNGSpringContextTests {

	// Mock the service dependencies(=ProductService is dependent on
	// productRepository)
	@Mock
	ProductRepository productRepository;

	@Mock
	ProductConverter productConverter;

	// Mock the service which is to be tested (Can't be an interface)
	@InjectMocks
	ProductService productService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(enabled = true)
	public void shouldReturnProductResModel_ForGivenId() throws ItemNotFoundException {

		Long id = 1L;
		// Given
		Product product = new Product(1L, "00001", "Pen", new BigDecimal(5), ShipmentType.LOCAL, "Ball Pen");
		ProductResponseModel productResponseModel = new ProductResponseModel(null, "00001", "Pen", new BigDecimal(5),
				ShipmentType.LOCAL, "Ball Pen", 10);

		when(this.productRepository.findById(id)).thenReturn(Optional.of(product));
		when(this.productConverter.productToProductDto(product)).thenReturn(productResponseModel);

		// When
		ProductResponseModel testProductResponseModel = this.productService.getProductById(1L);

		// Verify
		verify(this.productRepository).findById(id);
		verify(this.productConverter, times(1)).productToProductDto(any());

		// Then
		Assert.assertEquals(testProductResponseModel.getProductId(), "00001");
	}

	@Test(enabled = true, expectedExceptions = { ItemNotFoundException.class })
	public void getProductById_ShouldThrowItemNotFoundExcp() throws ItemNotFoundException {

		// Given
		Long id = 1L;

		when(this.productRepository.findById(id)).thenThrow(ItemNotFoundException.class);

		// When
		this.productService.getProductById(id);

		// Verify
		verify(this.productRepository).findById(id);
		verify(this.productConverter, times(0)).productToProductDto(any());
	}

	@Test(enabled = true, expectedExceptions = { IllegalArgumentException.class })
	public void getProductById_ShouldThrowIllegalArgumentExcp() throws ItemNotFoundException {

		// Given
		Long id = null;

		when(this.productRepository.findById(id)).thenThrow(IllegalArgumentException.class);

		// When
		this.productService.getProductById(id);

		// Verify
		verify(this.productRepository).findById(id);
		verify(this.productConverter, times(0)).productToProductDto(any());
	}

	@Test(enabled = true)
	public void shouldSaveProduct() {

		// Given
		ProductRequestModel productRequestModel = new ProductRequestModel(null, "00001", "Pen", new BigDecimal(5),
				ShipmentType.LOCAL, "Ball Pen");
		Product product = new Product(null, "00001", "Pen", new BigDecimal(5), ShipmentType.LOCAL, "Ball Pen");
		ProductResponseModel productResponseModel = new ProductResponseModel(null, "00001", "Pen", new BigDecimal(5),
				ShipmentType.LOCAL, "Ball Pen", 10);

		when(this.productConverter.productDtoToProduct(any(ProductRequestModel.class))).thenReturn(product);
		when(this.productRepository.save(any(Product.class))).thenReturn(product);
		when(this.productConverter.productToProductDto(any(Product.class))).thenReturn(productResponseModel);

		// When
		ProductResponseModel testProductResponseModel = this.productService.saveProduct(productRequestModel);

		// Verify
		verify(this.productConverter).productDtoToProduct(productRequestModel);
		verify(this.productRepository).save(product);
		verify(this.productConverter).productToProductDto(product);

		// Then
		Assert.assertEquals(testProductResponseModel.getProductId(), "00001");
	}

	@Test(enabled = true)
	public void shouldUpdateProductForTheGivenId() throws ItemNotFoundException {

		// Given
		ProductRequestModel productRequestModel = new ProductRequestModel(1L, "00001", "Pen", new BigDecimal(5),
				ShipmentType.LOCAL, "Ball Pen");
		Product product = new Product(1L, "00002", "Pen", new BigDecimal(5), ShipmentType.LOCAL, "Ball Pen");
		ProductResponseModel productResponseModel = new ProductResponseModel(1L, "00001", "Pen", new BigDecimal(5),
				ShipmentType.LOCAL, "Ball Pen", 10);

		when(this.productRepository.findById(productRequestModel.getId())).thenReturn(Optional.of(product));
		when(this.productRepository.save(any(Product.class))).thenReturn(product);
		when(this.productConverter.productToProductDto(any(Product.class))).thenReturn(productResponseModel);

		Assert.assertNotEquals(product.getProductId(), productRequestModel.getProductId());

		// When
		ProductResponseModel testProductResponseModel = this.productService.updateProduct(productRequestModel);

		// Verify
		verify(this.productRepository).findById(productRequestModel.getId());
		verify(this.productRepository).save(product);
		verify(this.productConverter).productToProductDto(product);

		// Then
		Assert.assertEquals(product.getProductId(), productRequestModel.getProductId());
		Assert.assertEquals(testProductResponseModel.getProductId(), product.getProductId());
	}

	@Test(enabled = true)
	public void shouldDeleteProductForTheGivenId() throws ItemNotFoundException {

		// Given
		Long id = 1L;
		Product product = new Product(1L, "00002", "Pen", new BigDecimal(5), ShipmentType.LOCAL, "Ball Pen");

		when(this.productRepository.findById(id)).thenReturn(Optional.of(product));

		// When
		boolean testResult = this.productService.deleteProduct(id);

		// Verify
		verify(this.productRepository).findById(id);
		verify(this.productRepository).delete(product);

		// Then
		Assert.assertEquals(testResult, true);
	}

	@Test(enabled = true)
	public void shouldFindAllProducts() throws ItemNotFoundException {

		// Given
		Product product1 = new Product(1L, "00001", "Pen", new BigDecimal(5), ShipmentType.LOCAL, "Ball Pen");

		ProductResponseModel productResponseModel1 = new ProductResponseModel(1L, "00001", "Pen", new BigDecimal(5),
				ShipmentType.LOCAL, "Ball Pen", 10);

		List<Product> productsList = Stream.of(product1).collect(Collectors.toUnmodifiableList());
		Set<ProductResponseModel> productsProductResponseModelsSet = Stream.of(productResponseModel1)
				.collect(Collectors.toUnmodifiableSet());

		when(this.productRepository.findAll()).thenReturn(productsList);
		when(this.productConverter.productToProductDto(product1)).thenReturn(productResponseModel1);

		// When
		Set<ProductResponseModel> testProductResponseModelsSet = this.productService.findAllProducts();

		// Verify
		verify(this.productRepository).findAll();
		verify(this.productConverter, times(1)).productToProductDto(any(Product.class));

		// Then
		Assert.assertEquals(testProductResponseModelsSet, productsProductResponseModelsSet);
	}

//	BDDMockito.given(this.productRepository.save(product)).will(invocation -> invocation.getArgument(0));
//	doReturn(new Product()).when(this.productRepository.save(any(Product.class)));

}
