#the base image to be used
FROM adoptopenjdk/openjdk11:latest
LABEL maintainer="saurabh@simbiotiktech.com"

#docker build --no-cache=true --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t products:latest .
ARG BUILD_DATE

ENV ACTIVE_PRODUCT_SERVICE_PROFILE="dev"

#specify variables to be reused
ARG JAR_FILE=target/product-service-0.0.1.jar
				
#specific folder to be created in container
VOLUME /service_resources

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
#files to be copied to container
ADD ${JAR_FILE} product-service.jar

#the port of the container to be exposed
EXPOSE 8124

#Set variables to environment
ENV welcome.message="from docker config - product-service"

#command and parameters that will be executed when a container runs
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=${ACTIVE_PRODUCT_SERVICE_PROFILE}", "/product-service.jar"]